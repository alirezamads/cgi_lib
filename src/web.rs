// mod web_upload;
use std::env;
use std::io::*;
use std::path::Path;
use std::borrow::Cow::Borrowed;
use urlencoding::decode;
use urlencoding::encode;

const HTT_COOKIE:       &str = "HTTP_COOKIE";
const CON_TYPE:         &str = "CONTENT_TYPE";
const REQ_METHOD:       &str = "REQUEST_METHOD";
const CON_LENGTH:       &str = "CONTENT_LENGTH";
const QUE_STRING:       &str = "QUERY_STRING";
const REQ_GET:          &str = "GET";
const REQ_POST:         &str = "POST";
const XWWW_FORM_POST:   &str = "application/x-www-form-urlencoded";
const FROM_DATA_POST:   &str = "multipart/form-data";


/// Cookies Name enum -----------------------------------------------------
/// NAME: The new name can contain the characters A-Z, a-z, 0-9, _ and -
/// EXPIRES: Format like: <WeekDay in three leter>, <Day-Month-Year> <hh:mm:ss GMT>
/// for more info https://datatracker.ietf.org/doc/html/rfc2616#section-3.3.1
pub enum CookiesKey
{
    NAME,       
    EXPIRES, 
    PATH,       
    DOMAIN
}
// ------------------------------------------------------------------------ 

pub struct Web
{
    _web_method:        String,
    _web_query_string:  String,
    _web_cookie:        String,
    _web_buffer:        Vec<u8>
}

impl Web
{
    pub fn web_handle() -> Web
    {
        let     _cookie         = Web::env_keys_value(HTT_COOKIE);
        let     _contant_type   = Web::env_keys_value(CON_TYPE);
        let mut _method         = Web::env_keys_value(REQ_METHOD);
        let mut _query_string   = String::new();
        let mut _buffer         = Vec::new();
        
        match _method.as_ref()
        {
            REQ_GET => 
            {
                _query_string = Web::env_keys_value(QUE_STRING);
            }
            REQ_POST => 
            {
                match &_contant_type[.. _contant_type.find(';').unwrap_or(_contant_type.len())]
                {
                    FROM_DATA_POST => 
                    { 
                        // let _boundary = &_contant_type[_contant_type.find("boundary=").unwrap_or(0) + 9 ..];
                        let mut _stdinr     = std::io::stdin();
                        let _buf_is_ok      = _stdinr.read_to_end(&mut _buffer).ok();
                        if _buf_is_ok == None
                        {
                            println!("Something wrong to read buffer.. try again..");
                        }
                        _query_string = Web::get_query_string(&_buffer);
                    }
                    XWWW_FORM_POST => 
                    { 
                        let _content_length: u64    = Web::env_keys_value(CON_LENGTH).parse().unwrap_or(0);
                        let _stdin                  = std::io::stdin();
                        let mut _buffer_stdin       = std::io::BufReader::new(_stdin.take(_content_length));
                        _buffer_stdin.read_line(&mut _query_string).unwrap();
                    }
                    _ => 
                    {
                        println!("Undefine method...");
                    }
                }
            }
            _ => 
            { 
                _method = "No method".to_string() 
            }
        }
        
        Web { _web_method: _method, _web_query_string: _query_string, _web_cookie: _cookie, _web_buffer: _buffer }
    }

    fn env_keys_value(_key_in: &str) -> String
    {
        let mut _env_variable   = env::vars();
        let mut _value          = String::new();
        for (_key, _val) in _env_variable
        {
            if _key == _key_in
            {
                _value = _val;
                return _value;
            }
        }
        "None".to_string()
    }
    

    // ============================================ GET AND SET ===============================================
    // Get Variable --------------------------------------------------------
    pub fn get_variable(&self, _variable_name_in: &str) -> String
    {
        // parse query string 
        let _query_parse_vec    = querystring::querify(&self._web_query_string);
        let mut _value          = String::new();

        for (_key, _val) in _query_parse_vec
        {
            if _key == _variable_name_in
            {
                _value = _val.to_string();
                _value = _value.replace("+", "%20"); // Some browsers pass space with '+' char
                _value = decode(&_value).unwrap().to_string();
                return _value;
            }
        }

        "None".to_string()
    }
    // ---------------------------------------------------------------------

    // Set Content ---------------------------------------------------------
    pub fn set_content_type(_content_type_in: &str)
    {
        println!("Content-type: {}\n", _content_type_in );
    }
    // ---------------------------------------------------------------------

    // Set Cookies ---------------------------------------------------------
    pub fn set_cookies(_cookie_key: CookiesKey, _cookie_value_in: &str)
    { 
        let _encode_value       = encode(_cookie_value_in);   // encode cookie
        println!("Set-Cookie: {}={}",
        match _cookie_key
        {
            CookiesKey::DOMAIN  => "domain",
            CookiesKey::EXPIRES => "expires",
            CookiesKey::NAME    => "name",
            CookiesKey::PATH    => "path",
        },
        _encode_value);
    }

    pub fn set_cookies_str(_cookie_key_in: &str, mut _cookie_value_in: &str)
    {
        let _encode_value       = encode(_cookie_value_in);   // encode cookie
        println!("Set-Cookie: {}={}", _cookie_key_in, _encode_value);
    }
    // ---------------------------------------------------------------------

    // Get Cookies ---------------------------------------------------------
    pub fn get_cookies(&self, _cookie_name: CookiesKey) -> String
    {
        if self._web_cookie != "None"
        {
            let _parse            = ginger::parse_cookies(&self._web_cookie);
            let mut _decode_value = String::new();
            for _names in _parse 
            {
                if self._web_cookie != "None"
                   && _names.as_ref().unwrap().name == match _cookie_name 
                    {
                        CookiesKey::DOMAIN  => "domain",
                        CookiesKey::EXPIRES => "expires",
                        CookiesKey::NAME    => "name",
                        CookiesKey::PATH    => "path",
                    }
                {
                    _decode_value = decode(_names.unwrap().value).unwrap_or(Borrowed("None")).to_string();
                    return _decode_value;
                }
            }
        }
        "None".to_string()
    }

    pub fn get_cookies_str(&self, _cookie_name: &str) -> String
    {
        if self._web_cookie != "None"
        {
            let _parse            = ginger::parse_cookies(&self._web_cookie);
            let mut _decode_value = String::new();
            for _names in _parse
            {
                if self._web_cookie != "None" 
                   && _names.as_ref().unwrap().name == _cookie_name
                {
                    _decode_value = decode(_names.unwrap().value).unwrap_or(Borrowed("None")).to_string();
                    return _decode_value;
                }
            }
        }
        "None".to_string()
    }
    // ---------------------------------------------------------------------
    // ========================================================================================================
    
    // ============================================ UPLOAD ====================================================
    // Seved files to path --------------------------------------------------
    pub fn save_files(&self, _path_in: &str) -> bool
    {
        let mut _upload_state: bool     = false; 
        if self._web_buffer.len() > 0  // We have a file
        {
            // Get Boundary From First Line -----------------------------------
            let mut _full_boundary_line                 = String::new();
            let mut _data_counter: usize                = 0;
            while _data_counter < self._web_buffer.len() - 1
            {   
                if &self._web_buffer[_data_counter .. _data_counter + 2] == [13, 10]
                {
                    break;
                }
                _full_boundary_line.push(self._web_buffer[_data_counter] as char);
                _data_counter = _data_counter + 1;
            }
            // ----------------------------------------------------------------

            // Extract Data Froms And Save ------------------------------------
            {   // Extract Data Block 
                let mut _contant_parts          = String::from_utf8_lossy(&self._web_buffer);
                let _vec_content                = _contant_parts.split(&_full_boundary_line);
                let _path                       = _path_in;

                for _contants in _vec_content 
                {
                    let mut _name               = Web::get_contant(_contants, "name");
                    let mut _file_name           = Web::get_contant(_contants, "filename");
                    let mut _contant_type       = Web::get_contant(_contants, "Content-Type:");

                    if _name != None //Valid Content
                    {
                        let _value              = Web::get_value(_contants, &mut _name, &mut _contant_type);
                        if _file_name != None // We have khown contant file and save it 
                        {
                            let mut _file_name_string       = String::from(&_file_name.unwrap());
                            _file_name_string.pop();        // remove double quotation from begin and end
                            _file_name_string.remove(0);   
                            let _save_file_name             = Path::new(&_file_name_string);
                            let _full_path                 = Path::new(_path).join(_save_file_name);
                            let _saved_file                 = std::fs::write(&_full_path, _value.unwrap()).ok();
                            if _saved_file != None
                            {
                                println!("File saved to {}", _full_path.to_str().unwrap() );
                            }
                            else
                            {
                                println!("Err: {:?}", _saved_file);
                                return false
                            }
                        }
                    }
                }
            }
        }
        _upload_state
    }
    // ---------------------------------------------------------------------

    // Get Query String from byte array ------------------------------------
    fn get_query_string(_vec_data: &Vec<u8>) -> String
    {
        let mut _que_strin              = String::new();
        let mut _upload_state: bool     = false; 

        // Get Boundary From First Line -----------------------------------
        let mut _full_boundary_line                 = String::new();
        let mut _data_counter: usize                = 0;
        while _data_counter < _vec_data.len() - 1
        {   
            if &_vec_data[_data_counter .. _data_counter + 2] == [13, 10]
            {
                break;
            }
            _full_boundary_line.push(_vec_data[_data_counter] as char);
            _data_counter = _data_counter + 1;
        }
        // ----------------------------------------------------------------

        // Extract Data Froms And Save ------------------------------------
        {   // Extract Data Block 
            let mut _contant_parts          = String::from_utf8_lossy(&_vec_data);
            let _vec_content                = _contant_parts.split(&_full_boundary_line);

            for _contants in _vec_content 
            {
                let mut _name               = Web::get_contant(_contants, "name");
                let mut _file_name           = Web::get_contant(_contants, "filename");
                if _name != None //Valid Content
                {
                    let _value              = Web::get_value(_contants, &mut _name, &mut None);
                    if _file_name == None // thats not a file content, thats a query    
                    {
                        // Add to Query string with query-string format 
                        let mut _name_string               = String::from(_name.unwrap());
                        // remove double quotation from begin and end
                        _name_string = _name_string[1.._name_string.len() - 1].to_string();
                        _que_strin.push_str(_name_string.as_ref());
                        _que_strin.push('=');
                        let mut _value_fix_str              = String::from(_value.unwrap_or("None".to_string()));
                        _value_fix_str = (&_value_fix_str[.. _value_fix_str.len() - 2]).to_string(); // remove "\r\n"
                        _que_strin.push_str(encode(&_value_fix_str).as_ref());
                        _que_strin.push('&');
                    }
                }
            }
        }
        _que_strin.pop(); //Pop last '&'
        _que_strin
    }
    // ---------------------------------------------------------------------

    // Get contant from data string ----------------------------------------
    fn get_contant(_contants: &str, _disparsm: &str) -> Option<String>
    {
        let mut _str_disparsm               = String::from(_disparsm);
        let _parm_pos                       = _contants.find(_disparsm);
        if _parm_pos == None { return None };
        let mut _index_name                 = &_contants[_parm_pos.unwrap() + _str_disparsm.len() + 1 .. ]; // +1 skip '=' or ' '
        let _index_twolines                 = _index_name.find("\r\n").unwrap(); // end of line
        _index_name = &_index_name[.._index_twolines];
        let _index_end                      = _index_name.find(';').unwrap_or(_index_name.len());
        Some(_index_name[.. _index_end].to_string())
    }
    // ---------------------------------------------------------------------

    // Get data value ------------------------------------------------------
    fn get_value(_contants: &str, _name: &mut Option<String>, _contant_type: &mut Option<String>) -> Option<String>
    {
        if *_name != None //Valid Content
        {
            if *_contant_type != None //We have khown contant file
            {
                let _find_pos               = _contants.find(_contant_type.as_ref().unwrap()).unwrap_or(0);
                let _contant_strlen        = _contant_type.as_ref().unwrap().len() ;
                return Some(_contants[_find_pos + _contant_strlen + 4 ..].to_string()); // +4 skip "/r/n/r/n"
            }
            let _find_pos                   = _contants.find("\r\n\r\n").unwrap_or(0);
            return Some(_contants[_find_pos + 4 ..].to_string()); // +4 skip "/r/n/r/n"
        }
        Some("None".to_string())
    }
    // ---------------------------------------------------------------------
    // ========================================================================================================

    // ============================================ GET Data Members ==========================================
    // Get Data Members ----------------------------------------------------
    pub fn get_method(&self) -> String      { self._web_method.to_string() }
    pub fn get_cookies_raw(&self) -> String { self._web_cookie.to_string() }
    pub fn get_qstring_raw(&self) -> String { self._web_query_string.to_string() }
    // pub fn get_buffer_raw(&self) -> Vec<u8> { self._web_buffer.to_vec() }
    // ---------------------------------------------------------------------
    // ========================================================================================================
}