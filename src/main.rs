mod web;
use crate::web::*;
use web::CookiesKey;

fn main() 
{
    
    // ---------------- set cookies -------------------
    // * first we should set cookies then set contenct //
    Web::set_cookies(CookiesKey::NAME, "alirrzs");
    Web::set_cookies_str("and_in","hello&hello");

    let _web    = Web::web_handle();

    // ------------- save uploaded files ---------------
    let _path   = "/Users/alirezamky/Documents/upload";
    _web.save_files(_path);

    // ---------------- set content -------------------
    Web::set_content_type("text/html");
    println!("<html>");
    println!("<head>");
    println!("    <title>cgi test</title>");
    println!("</head>");
    println!("<body>");
    // ---------------- get & post -------------------
    println!("    <h1>Text Box Value</h1>");
    println!("    <h2>Method: {}</h2>", _web.get_method()); // Method
    println!("        <p>A: {} <br></p>", _web.get_variable("a"));
    println!("        <p>B: {} <br></p>", _web.get_variable("b"));
    println!("        <p>C: {} <br></p>", _web.get_variable("c"));
    println!("        <p>Raw query string: {} <br></p>", _web.get_qstring_raw());

    // ---------------- cookies ----------------------
    println!("        <hr>");
    println!("    <h1>Cookies: </h1>");
    println!("        <p> {} : {} <br></p>", "name", _web.get_cookies(CookiesKey::NAME));
    println!("        <p> {} : {} <br></p>", "and_in", _web.get_cookies_str("and_in"));
    // --------- none value in cookies ----------------
    println!("        <p> {} : {} <br></p>", "no_one", _web.get_cookies_str("no_one"));
    println!("        <p>Raw cookies: {} <br></p>", _web.get_cookies_raw() );
    println!("</body>");
}


// Cookies: https://docstore.mik.ua/orelly/weblinux2/apache/ch16_04.html